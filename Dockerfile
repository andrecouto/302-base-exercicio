FROM java:8-jdk-alpine
COPY target/Api-Investimentos-0.0.1-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
EXPOSE 8080
CMD ["java", "-jar", "Api-Investimentos-0.0.1-SNAPSHOT.jar"]